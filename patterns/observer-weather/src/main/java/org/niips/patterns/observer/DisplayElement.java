/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.niips.patterns.observer;

/**
 *
 * @author panfilov.ms
 */
public interface DisplayElement {
    public void display();
}
