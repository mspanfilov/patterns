/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.niips.patterns.observer;

import java.util.ArrayList;
import java.util.Observable;

/**
 *
 * @author panfilov.ms
 */
public class WeatherData extends Observable{
    
    private ArrayList<java.util.Observer> observers;
    private float temp;
    private float humidity;
    private float pressure;

    public WeatherData() {
    }
    
    public void measurementChanged() {
        setChanged();
        notifyObservers();
    }
    
    public void setMeasurements(float temp, float humidity, float pressure){
        this.temp = temp;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementChanged();
    }

    public float getTemp() {
        return temp;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }
    
    
    
}
