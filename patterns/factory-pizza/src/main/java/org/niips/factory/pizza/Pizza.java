/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.niips.factory.pizza;

/**
 *
 * @author panfilov.ms
 */
public abstract class Pizza {
    String name;
    String dough;
    String sauce;
    
    public void prepare(){
        
    }
    
    public void bake(){
        
    }
    
    public void cut(){
        
    }
    
    public void box(){
        
    }
    
}
