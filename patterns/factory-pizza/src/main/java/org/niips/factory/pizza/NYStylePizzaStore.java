/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.niips.factory.pizza;

/**
 *
 * @author panfilov.ms
 */
public class NYStylePizzaStore extends PizzaStore{

    @Override
    Pizza createPizza(String type) {
        if (type.equals("cheese")){
            return new NYStyleCheesePizza();
        } else if (type.equals("pepperoni")){
            return new NYStylePeperoniPizza();
        } else if (type.equals("clam")){
            return new NYStyleClamPizza();
        } else if (type.equals("veggie")){
            return new NYStyleVeggiePizza();
        } else return null;
    }
    
}
