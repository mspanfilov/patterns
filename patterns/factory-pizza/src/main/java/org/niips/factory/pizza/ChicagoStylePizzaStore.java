/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.niips.factory.pizza;

/**
 *
 * @author panfilov.ms
 */
public class ChicagoStylePizzaStore extends PizzaStore{

    @Override
    Pizza createPizza(String type) {
        if (type.equals("cheese")){
            return new ChicagoStyleCheesePizza();
        } else if (type.equals("pepperoni")){
            return new ChicagoStylePeperoniPizza();
        } else if (type.equals("clam")){
            return new ChicagoStyleClamPizza();
        } else if (type.equals("veggie")){
            return new ChicagoStyleVeggiePizza();
        } else return null;
    }
    
}
