/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.niips.patterns.decorator;

/**
 *
 * @author panfilov.ms
 */
public abstract class Beverage {
    String description = "Unknown Beverage";
    enum CoffeeSize {SMALL, MEDIUM, BIG}
    CoffeeSize size;

    public Beverage(CoffeeSize size) {
        this.size = size;
    } 

    public CoffeeSize getSize() {
        return size;
    }

    public void setSize(CoffeeSize size) {
        this.size = size;
    }
    
    public String getDescription() {
        return description;
    }
    
    public abstract double cost();
}
