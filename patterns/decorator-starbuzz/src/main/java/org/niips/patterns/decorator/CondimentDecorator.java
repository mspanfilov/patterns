/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.niips.patterns.decorator;

/**
 *
 * @author panfilov.ms
 */
public abstract class CondimentDecorator extends Beverage{

    public CondimentDecorator(CoffeeSize size) {
        super(size);
    }
    @Override
    public  abstract String getDescription();
}
