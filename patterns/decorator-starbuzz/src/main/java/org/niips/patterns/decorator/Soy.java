/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.niips.patterns.decorator;

/**
 *
 * @author panfilov.ms
 */
public class Soy extends CondimentDecorator{
    Beverage beverage;

    public Soy(Beverage beverage) {
        super(beverage.getSize());
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.description + ", Soy";
    }

    @Override
    public double cost() {
        double size = 0.15;
        switch (beverage.getSize()) {
            case SMALL: size = 0.10;
            case BIG: size = 0.20;
        }
        return size + beverage.cost();
    }
    
}
