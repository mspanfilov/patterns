/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.niips.patterns.decorator;

/**
 *
 * @author panfilov.ms
 */
public class Mocha extends CondimentDecorator{
    Beverage beverage;

    public Mocha(Beverage beverage) {
        super(beverage.getSize());
        this.beverage = beverage;
    }
    
    @Override
    public String getDescription() {
        return beverage.getDescription() + ", Mocha";
    }

    @Override
    public double cost() {
        double size = 0.20;
        switch (beverage.getSize()) {
            case SMALL: size = 0.15;
            case BIG: size = 0.25;
        }
        return size + beverage.cost();
    }
    
}
