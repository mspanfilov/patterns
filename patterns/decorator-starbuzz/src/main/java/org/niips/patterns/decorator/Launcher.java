/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.niips.patterns.decorator;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import static org.niips.patterns.decorator.Beverage.CoffeeSize.BIG;
import static org.niips.patterns.decorator.Beverage.CoffeeSize.MEDIUM;
import static org.niips.patterns.decorator.Beverage.CoffeeSize.SMALL;

/**
 *
 * @author panfilov.ms
 */
public class Launcher {
    public static void main(String[] args) {
        Beverage beverage = new Espresso(SMALL);
        System.out.println(beverage.getDescription() + " $" + beverage.cost());
        
        Beverage beverage2 = new DarkRoast(MEDIUM);
        beverage2 = new Mocha(beverage2);
        beverage2 = new Mocha(beverage2);
        beverage2 = new Whip(beverage2);
        System.out.println(beverage2.getDescription() + " $" + beverage2.cost());
        
        Beverage beverage3 = new HouseBlend(BIG);
        beverage3 = new Soy(beverage3);
        beverage3 = new Mocha(beverage3);
        beverage3 = new Whip(beverage3);
        System.out.println(beverage3.getDescription() + " $" + beverage3.cost());
        
        int c;
        try {
            InputStream in = new LowerCaseInputStream(new BufferedInputStream(new FileInputStream("src/main/resources/test.txt")));
            while ((c = in.read()) > 0){
                System.out.print((char)c);
            }
            in.close();
        } catch(IOException e){
            e.printStackTrace();
        }
        
    }
}
